import java.util.ArrayList;
public class Product {
	
	private String name;
	private int price;
	private String category;
	private ArrayList<Example> examples = new ArrayList<Example>();
	private ArrayList<OrderLine> orderLine = new ArrayList<OrderLine>();
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
	return name;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getPrice() {
		return price;
	}
	public void setCategory(String c) {
		this.category = c;
	}
	public String getCategory() {
		return category;
	}
	public void addExamples(Example examples) {
		this.examples.add(examples);
	}
	public ArrayList<Example>  getExamples() {
		return examples;
	}
	
	
	
}
