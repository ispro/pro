import java.util.ArrayList;
public class OrderLine {
	private Product product;
	private Order order;
	private int number;
	private int quantity;
	private ArrayList<Example> examples;
	
	public void setProduct(Product product) {
		this.product = product;
	}
	public Product getProduct() {
		return this.product;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Order getOrder() {
		return this.order;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getNumber() {
		return this.number;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getQuantity() {
		return this.quantity;
	}
	
	
	

}
