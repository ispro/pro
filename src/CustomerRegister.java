import java.util.ArrayList;

public class CustomerRegister {
	private ArrayList<Customer> customerList = new ArrayList<Customer>();
	public Order tempOrder;

	public ArrayList<Customer> getCustomerList() {
		return this.customerList;
	}

	public void setCustomerList(ArrayList<Customer> customerList) {
		this.customerList = customerList;
	}

	public void addCustomer(Customer newCustomer) {
		this.customerList.add(newCustomer);
	}

	public Customer findCustomerWithNumber(String customerNr) {
		for (Customer c : this.customerList) {
			if (c.getCustomerNr().equals(customerNr)) {
				return c;
			}
		}
		return null;
	}

	public Customer findCustomerWithInfo(String name, String address) {
		for (Customer c : this.customerList) {
			if (c.getName().equals(name) && c.getAdress().equals(address)) {
				return c;
			}
		}
		return null;
	}

	public boolean removeCustomer(String customerNr) {
		Customer c = this.findCustomerWithNumber(customerNr);
		if (c != null) {
			this.customerList.remove(c);
			return true;
		}
		return false;
	}

	public int addOrder(String customerNr) {
		Customer cst = this.findCustomerWithNumber(customerNr);
		Order ord = tempOrder;
		if (cst != null) { //customer exist
			ord.setCustomer(cst);
			cst.addOrder(ord);
			tempOrder = null;
			int number =  ord.getOrderId();
			return number;
		}
		return -1; //customer NOT exist
	}
	
	public boolean removeOrder(int orderID, String customerNr) {
		Customer c = this.findCustomerWithNumber(customerNr);
		if (c != null) {
			Order o = getCustomerOrder(orderID, c);
			if (o != null) {
				c.getOrders().remove(o);
				return true;
			}
		}
		return false;
	}

	public boolean removeOrderLine(int orderLineID, int orderID, String customerNr) {
		Customer c = this.findCustomerWithNumber(customerNr);
		if (c != null) {
			Order o = getCustomerOrder(orderID, c);
			return o.removeOrderItem(orderLineID);
		}
		return false;
	}
	
	private Order getCustomerOrder(int orderID, Customer c) {
		ArrayList<Order> orders = c.getOrders();
		if (!orders.isEmpty()) {
			for (Order o : orders) {
				if (o.getOrderId() == orderID) {
					return o;
				}
			}
		}
		return null;
	}
}
