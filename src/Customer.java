import java.util.ArrayList;
public class Customer {
	
	private String name;
	private String adress;
	private String customerNr;
	private ArrayList<Order> order = new ArrayList<Order>();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAdress(String adress) {
		this.adress = adress;
	}
	
	public String getAdress() {
		return adress;
	}
	
	public void setCustomerNr(String customerNr) {
		this.customerNr = customerNr;
	}
	public String getCustomerNr() {
		return customerNr;
	}
	public void setOrder(ArrayList order) {
		this.order = order;
	}
	public ArrayList<Order> getOrders() {
		return order;
	}
	
	public void addOrder(Order newOrder) {
		order.add(newOrder);
	}
	
	
	
	
	

}
