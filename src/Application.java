import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class Application {

	private JFrame frame;
	private CustomerRegister cr;
	private ProductRegister pr;
	private Controller controller;
	private JTextField customerNameTextField;
	private JTextField customerAddressTextField;
	private JTextField customerNumberTextField;
	private JLabel responseLabel;
	private JTextField orderIdtextField;
	private JTextField orderItemTextField;
	JTextArea textArea;
	private JTextField prisTxtField;
	private JTextField CategoriTextField;
	private JTextField NameTextField;
	private JTextField OrderItemCountTextField;
	private JTextField itemCountTextField;
	private JTextField ProductTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application window = new Application();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Application() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		cr = new CustomerRegister();
		pr = new ProductRegister();
		controller = new Controller(cr, pr, frame);
		
				
		frame.setBounds(100, 100, 750, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		customerNameTextField = new JTextField();
		customerNameTextField.setBounds(107, 97, 130, 26);
		frame.getContentPane().add(customerNameTextField);
		customerNameTextField.setColumns(10);
		
		JLabel lblNamn = new JLabel("Namn:");
		lblNamn.setBounds(12, 102, 61, 16);
		frame.getContentPane().add(lblNamn);
		
		JLabel lblAdress = new JLabel("Adress:");
		lblAdress.setBounds(12, 130, 61, 16);
		frame.getContentPane().add(lblAdress);
		
		customerAddressTextField = new JTextField();
		customerAddressTextField.setBounds(107, 125, 130, 26);
		frame.getContentPane().add(customerAddressTextField);
		customerAddressTextField.setColumns(10);
		
		JButton btnFindCustomer = new JButton("Sök med nummer");
		btnFindCustomer.setBounds(6, 51, 135, 29);
		frame.getContentPane().add(btnFindCustomer);
	
		JButton btnTaBortCustomer = new JButton("Ta bort");
		btnTaBortCustomer.setBounds(153, 51, 83, 29);
		frame.getContentPane().add(btnTaBortCustomer);

		JButton btnCreateCustomer = new JButton("Skapa");
		btnCreateCustomer.setBounds(12, 158, 135, 29);
		frame.getContentPane().add(btnCreateCustomer);
		
		customerNumberTextField = new JTextField();
		customerNumberTextField.setBounds(107, 13, 130, 26);
		frame.getContentPane().add(customerNumberTextField);
		customerNumberTextField.setColumns(10);
		
		JLabel lblKundnummer = new JLabel("Kundnummer:");
		lblKundnummer.setBounds(12, 18, 96, 16);
		frame.getContentPane().add(lblKundnummer);
		
		JButton btnEditCustomer = new JButton("Ändra");
		btnEditCustomer.setBounds(154, 158, 83, 29);
		frame.getContentPane().add(btnEditCustomer);
		
		 responseLabel = new JLabel("Response:---------------------");
		responseLabel.setBounds(423, 209, 291, 16);
		frame.getContentPane().add(responseLabel);
		
		JButton addOrder = new JButton("Lägg till order");
		addOrder.setBounds(624, 51, 117, 29);
		frame.getContentPane().add(addOrder);
		
		JButton deleteOrder = new JButton("Ta bort order");
		deleteOrder.setBounds(624, 92, 117, 29);
		frame.getContentPane().add(deleteOrder);
		
		JLabel lblNewLabel = new JLabel("Order ID:");
		lblNewLabel.setBounds(541, 23, 61, 16);
		frame.getContentPane().add(lblNewLabel);
		
		orderIdtextField = new JTextField();
		orderIdtextField.setBounds(614, 18, 130, 26);
		frame.getContentPane().add(orderIdtextField);
		orderIdtextField.setColumns(10);
		
		JButton addOrderItemBtn = new JButton("Lägg till köplist");
		addOrderItemBtn.setBounds(352, 125, 151, 29);
		frame.getContentPane().add(addOrderItemBtn);
		
		JButton removeOrderItemBtn = new JButton("Ta bort från köplist");
		removeOrderItemBtn.setBounds(352, 158, 151, 29);
		frame.getContentPane().add(removeOrderItemBtn);
		
		orderItemTextField = new JTextField();
		orderItemTextField.setBounds(365, 13, 130, 26);
		frame.getContentPane().add(orderItemTextField);
		orderItemTextField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Produkt ID:");
		lblNewLabel_1.setBounds(297, 18, 71, 16);
		frame.getContentPane().add(lblNewLabel_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 220, 300, 252);
		frame.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
		
		prisTxtField = new JTextField();
		prisTxtField.setBounds(611, 446, 130, 26);
		frame.getContentPane().add(prisTxtField);
		prisTxtField.setColumns(10);
		
		CategoriTextField = new JTextField();
		CategoriTextField.setBounds(611, 388, 130, 26);
		frame.getContentPane().add(CategoriTextField);
		CategoriTextField.setColumns(10);
		
		NameTextField = new JTextField();
		NameTextField.setBounds(399, 388, 130, 26);
		frame.getContentPane().add(NameTextField);
		NameTextField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Pris:");
		lblNewLabel_2.setBounds(541, 451, 61, 16);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Kategori:");
		lblNewLabel_3.setBounds(541, 393, 61, 16);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Namn:");
		lblNewLabel_4.setBounds(326, 393, 71, 16);
		frame.getContentPane().add(lblNewLabel_4);
		
		JButton btnLggTillProdukt = new JButton("Lägg till");
		btnLggTillProdukt.setBounds(624, 329, 117, 29);
		frame.getContentPane().add(btnLggTillProdukt);
		
		JLabel lblNewLabel_5 = new JLabel("Produktregister list");
		lblNewLabel_5.setBounds(506, 260, 130, 16);
		frame.getContentPane().add(lblNewLabel_5);
		
		JButton btnRemoveProdukt = new JButton("Ta bort");
		btnRemoveProdukt.setBounds(399, 329, 117, 29);
		frame.getContentPane().add(btnRemoveProdukt);
		
		JButton btnAndraProdukt = new JButton("Ändra");
		btnAndraProdukt.setBounds(624, 288, 117, 29);
		frame.getContentPane().add(btnAndraProdukt);
		
		JButton btnSokProdukt = new JButton("Sök med namn");
		btnSokProdukt.setBounds(399, 288, 117, 29);
		frame.getContentPane().add(btnSokProdukt);
		
		OrderItemCountTextField = new JTextField();
		OrderItemCountTextField.setBounds(365, 51, 130, 26);
		frame.getContentPane().add(OrderItemCountTextField);
		OrderItemCountTextField.setColumns(10);
		
		itemCountTextField = new JTextField();
		itemCountTextField.setBounds(399, 446, 130, 26);
		frame.getContentPane().add(itemCountTextField);
		itemCountTextField.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Antal:");
		lblNewLabel_6.setBounds(326, 451, 61, 16);
		frame.getContentPane().add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Antal:");
		lblNewLabel_7.setBounds(297, 56, 61, 16);
		frame.getContentPane().add(lblNewLabel_7);
		
		ProductTextField = new JTextField();
		ProductTextField.setBounds(365, 97, 130, 26);
		frame.getContentPane().add(ProductTextField);
		ProductTextField.setColumns(10);
		
		JLabel lblNamn_1 = new JLabel("Namn:");
		lblNamn_1.setBounds(292, 102, 61, 16);
		frame.getContentPane().add(lblNamn_1);
		
		// Find customer from customer register list
		btnFindCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String customerNumber = customerNumberTextField.getText();
				ArrayList<String> foundCustomer = controller.findCustomer(customerNumber);
				if (foundCustomer != null) {
					customerNameTextField.setText(foundCustomer.get(0));
					customerAddressTextField.setText(foundCustomer.get(1));
					//Clear textArea
					textArea.selectAll();
					textArea.replaceSelection("");
					//Show order info in textArea
					for(int i = 2;i<foundCustomer.size();i++) {
						textArea.append(foundCustomer.get(i));
					}
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Finns inte!");
				}
			}
		});
		
		// Add customer to customer register list
		btnCreateCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerName = customerNameTextField.getText();
				if(customerName.isEmpty()) {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Skriv ett namn!");
					return;
				}
				String customerAddress = customerAddressTextField.getText();
				String result = controller.createCustomer(customerName, customerAddress);
				if(result != null) {//Could create a new customer
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Din kundnummer är: " + result);
				} else {//if customer already exist show ERROR
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Misslyckades!");
				}
			}
		});
		

		// Remove  customer from customer register list
		btnTaBortCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerNumber = customerNumberTextField.getText();
				boolean removed = controller.removeCustomer(customerNumber);
				if(removed) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Tog bort!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel kundnummer!");
				}
			}
		});
		

		// Edit customer info
		btnEditCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerNumber = customerNumberTextField.getText();
				String customerName = customerNameTextField.getText();
				String customerAddress = customerAddressTextField.getText();
				boolean edited = controller.editCustomer(customerName, customerAddress, customerNumber);
				if(edited) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel kundnummer!");
				}
			}
		});
		
		// Remove order from the customer order list
		deleteOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int orderNumber =Integer.parseInt(orderIdtextField.getText()) ;//convert string to int
				String customerNumber = customerNumberTextField.getText();
				boolean removed = controller.removeOrder(orderNumber, customerNumber);
				if(removed) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel kundnummer eller orderNummer!");
				}
			}
		});

		// Add order to customer
		addOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerNumber = customerNumberTextField.getText();
				int orderNumber = controller.addOrder(customerNumber);

				if(orderNumber == -2) {//A product has to be added first
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Lägg till en produkt först!");
				} else if(orderNumber == -1){//customer NOT exist
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel kundnummer!");
				} else {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Ordernummer är: " + orderNumber);

				}
			}
		});
		
		// Add product item to shop list
		addOrderItemBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sProductCount = OrderItemCountTextField.getText();
				String productName = ProductTextField.getText();
				if(productName.isEmpty() || sProductCount.isEmpty()) {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Namn/Antal Textfältet är tomt");
					return;
				}
				int count;
				try {
					count = Integer.parseInt(sProductCount) ;//convert string to int
			    } catch (NumberFormatException nfe) {
			    		responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel nummer format");
					return;	
			    }
				int number = controller.addOrderLine(productName, count);
				if (number == -1) {//Product not found
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel Namn!");
					return;
				}
				if (number == -2) {//Not enough number of product in database
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Finns inte "+count+"st i lager!");
					return;
				}
				textArea.append("Beställt produktnummer: " + number + "\n");
			}
		});
		
		// Remove product item from shop list
		removeOrderItemBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerNumber = customerNumberTextField.getText();
				int orderItemNumber,orderNumber;
				if(orderItemTextField.getText().isEmpty()|| orderIdtextField.getText().isEmpty()) {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("ID Textfältet är tomt");
					return;
				}
				try {
					 orderItemNumber = Integer.parseInt(orderItemTextField.getText()) ;//convert string to int
					 orderNumber =Integer.parseInt(orderIdtextField.getText()) ;//convert string to int
			    } catch (NumberFormatException nfe) {
			    		responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel nummer format");
					return;	
			    }
				boolean removed = controller.removeOrderLine(orderItemNumber,orderNumber, customerNumber);
				if(removed) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel kundnummer eller ordernummer!");
				}
			}
		});
		
		// Add product to ProductRegister list
		btnLggTillProdukt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = NameTextField.getText();
				String categori = CategoriTextField.getText();
				String sPrice = prisTxtField.getText();
				String sCount = itemCountTextField.getText();
				int price,count;
				if(name.isEmpty() || categori.isEmpty() || sPrice.isEmpty() || sCount.isEmpty()) {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Textfältet är tomt");
					return;
				}
				try {
					 price = Integer.parseInt(sPrice);//convert string to int
					 count = Integer.parseInt(sCount);//convert string to int
			    } catch (NumberFormatException nfe) {
			    		responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel nummer format");
					return;	
			    }
				boolean added = controller.addProductRegister(name, categori, price, count);
				if(added) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Misslyckades!");
				}
			}
		});
		
		// Remove product from ProductRegister list
		btnRemoveProdukt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = NameTextField.getText();
				if(name.isEmpty()) {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Namn Textfältet är tomt");
					return;
				}
				boolean removed = controller.removeProductRegisterItem(name);
				if(removed) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Misslyckades!");
				}
			}
		});
		
		// Search product from ProductRegister list
		btnSokProdukt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = NameTextField.getText();
				if(name.isEmpty()) {
					responseLabel.setText("Textfältet är tomt");
				}
				ArrayList<String> foundProduct = controller.findProductRegisterItem(name);
				if (foundProduct != null) {
					CategoriTextField.setText(foundProduct.get(0));
					prisTxtField.setText(foundProduct.get(1));
					itemCountTextField.setText(foundProduct.get(2));
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Misslyckades!");
				}
			}
		});
		
		// Change a product of ProductRegister list
		btnAndraProdukt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = NameTextField.getText();
				String categori = CategoriTextField.getText();
				String sPrice = prisTxtField.getText();
				String sCount = itemCountTextField.getText();
				if(name.isEmpty() || categori.isEmpty() || sPrice.isEmpty()  || sCount.isEmpty()) {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Textfältet är tomt");
					return;
				}
				int price, count;
				try {
					 price = Integer.parseInt(sPrice);//convert string to int
					 count = Integer.parseInt(sCount);//convert string to int
			    } catch (NumberFormatException nfe) {
			    		responseLabel.setForeground(Color.RED);
					responseLabel.setText("Fel nummer format");
					return;	
			    }
				boolean changed = controller.EditProductRegister(name, categori, price, count);
				if(changed) {
					responseLabel.setForeground(Color.BLACK);
					responseLabel.setText("Klart!");
				} else {
					responseLabel.setForeground(Color.RED);
					responseLabel.setText("Misslyckades!");
				}
			}
		});
	}
}
