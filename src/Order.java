import java.util.ArrayList;

public class Order {
	
	private int orderId;
	private int delivaryDate;
	private ArrayList<OrderLine> orderLine = new ArrayList<OrderLine>();
	private Customer owner;
	
	public void setOrderId(int id) {
		this.orderId = id;
	}
	public int getOrderId() {
		return this.orderId;
	}
	public void setDeliveriDate(int date) {
		this.delivaryDate = date;
	}
	public int getDeliveryDate() {
		return this.delivaryDate;
	}
	public Customer getCustomr() {
		return this.owner;
	}

	public void setCustomer(Customer owner) {
		this.owner = owner;
	}
	
	public ArrayList<OrderLine> getOrderLines() {
		return orderLine;
	}
	
	public void addOrderItem(OrderLine newOrder) {
		orderLine.add(newOrder);
	}
	
	public boolean removeOrderItem(int id) {
		if (!orderLine.isEmpty()) {
			for (OrderLine oi : orderLine) {
				if (oi.getNumber() == id) {
					orderLine.remove(oi);
					return true;
				}
			}
		}
		return false;
	}
}
