import java.util.ArrayList;

public class ProductRegister {

	private ArrayList<Product> productList = new ArrayList<Product>();
	
	public ArrayList<Product> getProductList() {
		return this.productList;
	}
	
	public Product findProduct(String name) {
		for (Product p : this.productList) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}
	
	public void addProduct(Product newProduct) {
		this.productList.add(newProduct);
	}

	public Product findProductWithNumber(String productName) {
		for (Product p : this.productList) {
			if (p.getName().equals(productName)) {
				return p;
			}
		}
		return null;
	}

	public boolean removeProduct(String productNr) {
		Product p = this.findProductWithNumber(productNr);
		if (p != null) {
			this.productList.remove(p);
			return true;
		}
		return false;
	}

	
	}	

