import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JFrame;


public class Controller {
	
	JFrame frame;
	CustomerRegister custReg;
	ProductRegister prodRegister;
	
	public Controller(CustomerRegister cr, ProductRegister pr, JFrame frame) {
		this.custReg = cr;
		this.prodRegister = pr;
		this.frame = frame;
	}

	public String createCustomer(String name, String address) {
		//check if there is  a register customer with this info
		Customer existCustomer = custReg.findCustomerWithInfo(name, address);
		if(existCustomer != null) {//Return null if there is a customer with this name and address
			return null;
		}
		Customer c = new Customer();
		c.setAdress(address);
		c.setName(name);
		String randomNumber = getRandomStringNumber();
		c.setCustomerNr(randomNumber);
		custReg.addCustomer(c);//  customerRegister
		return randomNumber;
	}

	public boolean removeCustomer(String customerNr) {
		boolean success = custReg.removeCustomer(customerNr);
		return success;
	}

	//Edit a customer info
	public boolean editCustomer(String name, String address, String customerNr) {
		Customer c = custReg.findCustomerWithNumber(customerNr);
		if (c == null ) {
			 return false;
		 }
		c.setAdress(address);
		c.setName(name);
		return true;
	}
	
	//Find a customer info with order list info
	public ArrayList<String> findCustomer(String customerNr) {
		Customer customer = custReg.findCustomerWithNumber(customerNr);
		if (customer != null ) {
			ArrayList<String> info = new ArrayList<String>();
			info.add(customer.getName());
			info.add(customer.getAdress());
			//order info
			if(!customer.getOrders().isEmpty()) {
				for(Order o: customer.getOrders()) {
					info.add("Order ID: "+o.getOrderId() + "\n");
					int price = 0;
					for(OrderLine ol: o.getOrderLines()) {
						price += ol.getProduct().getPrice() * ol.getQuantity();
						info.add(ol.getProduct().getName()+" pris: "+ ol.getProduct().getPrice()  +" quantity: "+ ol.getQuantity()  +" id:"+ol.getNumber() + "\n");
					}
					info.add("    Totalbelopp: "+price + "\n\n");
				}
			}
			 return info;
		 } else {
			return null;
		 }
	}
	
	//Get all orders of a customer
	public ArrayList<Order> getCustomerOrders(String customerNr) {
		Customer customer = custReg.findCustomerWithNumber(customerNr);
		ArrayList<Order> orders = customer.getOrders();
		return orders;
	}
	
	//Add order to a customer 
	public int addOrder(String customerNr) {
		if (custReg.tempOrder == null) { // Not add any NULL order to customer
			return -2;
		} 
		//reduce number of a product example from database, for each product
		for(OrderLine ordLine: custReg.tempOrder.getOrderLines()) {
			Product orderProduct = ordLine.getProduct();
			String orderProductName = orderProduct.getName();
			
			Product productInDB = prodRegister.findProduct(orderProductName);
			int orderdQuantity = ordLine.getQuantity();
			for(int i = productInDB.getExamples().size(); i > 0 && orderdQuantity>0; i--) {
				orderdQuantity--;
				int index = i-1;
				productInDB.getExamples().remove(index);
			}
		}
		
		return custReg.addOrder(customerNr);
	}
	
	//Remove a order from a customer order list
	public boolean removeOrder(int orderID, String customerNr) {
		return custReg.removeOrder(orderID, customerNr);
	}
	
	//Add a product to an order list for a customer
	public int addOrderLine(String productName, int count) {
		Product p = prodRegister.findProduct(productName);
		//Product not found
		if(p == null) {
			return -1;
		}
		if(p.getExamples().size() < count) {//Not enough number of product in Database
			return -2;
		}
		//when first item starts to add, we create a temp-order object
		// to keep all orderLine and add them to a customer's list later.
		if(custReg.tempOrder == null) {
			custReg.tempOrder = createOrder();
		}
		OrderLine orderLine = createOrderLine(productName, count);
		//Product not found
		if(orderLine == null) {
			return -1;
		}
		custReg.tempOrder.addOrderItem(orderLine);
		return orderLine.getNumber();
	}
	
	//Remove a product from an order list of a customer
	public boolean removeOrderLine(int orderLineID,int orderID, String customerNr) {
		return custReg.removeOrderLine(orderLineID, orderID, customerNr);
	}
	
	//Add a product to ProductRegister
	public boolean addProductRegister(String name, String categori, int price, int count) {
		Product product = new Product();
		product.setName(name);
		product.setPrice(price);
		product.setCategory(categori);
		for (int i = 0; i < count; i++) {
			Example ex = new Example();
			ex.setSerialNr(getRandomNumber());
			product.addExamples(ex);
		}
		prodRegister.addProduct(product);
		return true;
	}
	
	//Remove product from ProductRegister
	public boolean removeProductRegisterItem(String name) {
		Product p = prodRegister.findProduct(name);
		if (p != null ) {
			prodRegister.getProductList().remove(p);
			return true;
		}
		return false;
	}

	//Find product from ProductRegister
	public ArrayList<String> findProductRegisterItem(String name) {
		Product p = prodRegister.findProduct(name);
		if (p != null ) {
			ArrayList<String> info = new ArrayList<String>();
			info.add(p.getCategory());
			String prc = String.valueOf(p.getPrice());// int to string
			String expSize = String.valueOf(p.getExamples().size());// int to string
			info.add(prc);
			info.add(expSize);
			return info;
		}
		return null;
	}

	//Edit a product in ProductRegister
	public boolean EditProductRegister(String name, String categori, int price, int count) {
		Product p = prodRegister.findProduct(name);
		if (p != null ) {
			p.setCategory(categori);
			p.setName(name);
			p.setPrice(price);
			if (p.getExamples().size() < count) {// add extras product example
				int extra = count - p.getExamples().size();
				for (int i = 0; i < extra; i++) {
					Example ex = new Example();
					ex.setSerialNr(getRandomNumber());
					p.addExamples(ex);
				}
			} else {
				//Not covered!
			}
			return true;
		}
		return false;
	}
	
	//Create an order object with random id number
	private Order createOrder() {
		Order order = new Order();
		order.setOrderId(getRandomNumber());
		return order;
	}
	
	//Create an order line with product info from productRegister to an order
	private OrderLine createOrderLine(String name, int count) {
		OrderLine orderLine = new OrderLine();
		Product p = prodRegister.findProduct(name);
		if (p != null ) {
			orderLine.setProduct(p);
			orderLine.setQuantity(count);
			orderLine.setNumber(getRandomNumber());
			return orderLine;
		}
		return null;
	}
	
	//Change int to String
	private String getRandomStringNumber() {
		return String.valueOf(getRandomNumber());
	}
	//Create random number
	private int getRandomNumber() {
		int min = 100;
		int max = 99999;
		return ThreadLocalRandom.current().nextInt(min, max);
	}
	
	
}
